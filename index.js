const request = require('request');
const fs = require('fs');
const getData = (url, cb) => {
    const options = {
        uri: url,
        headers: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
        }
    };

    // Return new promise 
    return new Promise(function(resolve, reject) {
        // Do async job
           request.get(options, function(err, resp) {
               if (err) {
                   reject(err);
               } else {
                   resolve(JSON.parse(resp.body));
               }
           })
       })

}

const getGithubProfile = (username, cb) =>{
    // get github profile
    let obj = {}
    getData('https://api.github.com/users/'+ username)
    .then(user => {
        const { name, company, avatar_url, followers_url } = user
        Object.assign(obj, {
            name,
            company,
            avatar_url
        });
        return getData(user.followers_url)
    })
    .then(followers => {
        obj.followers = followers;
        return getData(`https://api.github.com/users/${username}/following`)
    })
    .then(followings => {
        obj.followings = followings;
        return getData(`https://api.github.com/users/${username}/starred`)
    })
    .then(starred => {
        obj.starred = starred;
        fs.writeFile(`./${username}.json`, JSON.stringify(obj), (err) => {
            if (err) {
                cb(err);
            } else {
                cb(null, obj)
            }
        });
    })
}

getGithubProfile('vophihungvn', (err, data) => {
    console.log(err, data)
})